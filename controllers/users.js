import bcrypt from 'bcryptjs';
import redis from 'redis';

import {ErrorResponse} from '../utils/errorResponse.js';
import tokenGenerator from '../utils/tokenGenerator.js';

//Set Port
const REDIS_PORT = process.env.PORT || 6379;
const client = redis.createClient(REDIS_PORT, process.env.HOST);

export class UsersController {
    constructor() {
    }

    // @desc Create user on DB
    // @route POST /api/v1/users/
    // @access Public
    async createUser(req, res, next) {
        try {
            const {name, lastName, username, password} = req.body;

            client.keys('*', (err, keys) => {
                if (err) return console.log(err);
                for (let key of keys) {
                    if (key === username) {
                        return next(new ErrorResponse('Username exist', 400));
                    }
                }
            });

            if (!username || !password || !lastName || !name) {
                return next(new ErrorResponse('Invalid params', 400));
            }

            const salt = process.env.PASSWORD_SALT;

            const passwordHash = await bcrypt.hash(password, parseInt(salt));

            const data = {
                name,
                lastName,
                username,
                passwordHash,
            };

            client.setex(username, 3600, JSON.stringify(data));

            const resPayload = {
                success: true,
                data: {username, ...tokenGenerator({username})},
            };
            res.status(200).send(resPayload);
        } catch (e) {
            console.error(e);
            res.status(500);
        }
    }

    // @desc Verify if user with the username exists
    // @route GET /api/v1/users/:username
    // @access Public
    async getUserByUsername(req, res, next) {
        try {
            const {username} = req.params;
            if (!username) {
                return next(new ErrorResponse('Please provide an username', 400));
            }

            client.keys('*', (err, keys) => {
                if (err) return console.log(err);
                let usernames = [];
                for (let key of keys) {
                    usernames.push(key)
                }
                let userExists = usernames.filter(exist => {
                    return exist === username;
                });
                if (userExists.length === 0) {
                    return next(new ErrorResponse('Username no exists', 404));
                }
            });

            client.get(username, (err, reply) => {
                let user = JSON.parse(reply)
                res.status(200).send(
                    {
                        success: true,
                        data: {
                            username,
                            name: user.name,
                            lastName: user.lastName,
                            password: user.passwordHash
                        }
                    });

            });
        } catch (e) {
            console.error(e);
            res.status(500);
        }
    }

    // @desc Edit user on DB
    // @route PUT /api/v1/users/:username
    // @access Private
    async updateUserByUsername(req, res, next) {
        try {

            const {name, lastName, username, password} = req.body;

            if (!username) {
                return next(new ErrorResponse('Please provide an username', 400));
            }

            client.keys('*', (err, keys) => {
                if (err) return console.log(err);
                let usernames = [];
                for (let key of keys) {
                    usernames.push(key)
                }
                let userExists = usernames.filter(exist => {
                    return exist === username;
                });
                if (userExists.length === 0) {
                    return next(new ErrorResponse('Username no exists', 404));
                }
            });

            client.get(username, async (err, reply) => {
                let user = JSON.parse(reply)

                const match = await bcrypt.compare(password, user['passwordHash']);

                if (!match) {
                    const salt = process.env.PASSWORD_SALT;
                    const passwordHash = await bcrypt.hash(password, parseInt(salt));
                    const data = {
                        name: user.name,
                        lastName: user.lastName,
                        username: user.username,
                        passwordHash,
                    };

                    client.setex(username, 3600, JSON.stringify(data));
                }

                if (lastName !== user.lastName) {
                    const data = {
                        name: user.name,
                        lastName,
                        username: user.username,
                        passwordHash: user.passwordHash,
                    };
                    client.setex(username, 3600, JSON.stringify(data));
                }

                if (name !== user.name) {
                    const data = {
                        name,
                        lastName: user.lastName,
                        username: user.username,
                        passwordHash: user.passwordHash,
                    };
                    client.setex(username, 3600, JSON.stringify(data));
                }
                res.status(200).send(
                    {
                        success: true,
                        data: {
                            username,
                            name: user.name !== name ? name : user.name,
                            lastName: user.lastName !== lastName ? lastName : user.lastName,
                        }
                    });

            });

        } catch (e) {
            console.error(e);
            res.status(500);
        }
    }

    // @desc Get user by credentials and validate password
    // @route GET /api/v1/users/
    // @access Public
    async loginUser(req, res, next) {
        try {
            const {username, password} = req.headers;

            if (client.exists(username, (err, reply) => {
                if (reply === 0) {
                    return next(new ErrorResponse('Invalid user credentials', 401));
                }
            }))
                client.get(username, async (err, reply) => {
                    let user = JSON.parse(reply)

                    const match = await bcrypt.compare(password, user['passwordHash']);

                    if (!match) {
                        return next(new ErrorResponse('Invalid user credentials', 401));
                    }

                    const resPayload = {
                        success: true,
                        data: {
                            username: user.username,
                            name: user.name,
                            lastName: user.lastName,
                            ...tokenGenerator({username: user.username}),
                        },
                    };

                    res.status(200).send(resPayload);

                })

        } catch (e) {
            console.error(e);
            res.status(500);
        }
    }

}
